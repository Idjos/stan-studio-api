const express = require('express');
const router = require('express-promise-router')();
const passport = require('passport');
const passportConf = require('../passport');

const RecordsController = require('../controllers/records');
const passportSignin = passport.authenticate('local', {session:false});
const passportJWT = passport.authenticate('jwt', {session:false});

router.route('/getRecords')
    .get(RecordsController.getRecords);

router.route('/addRecords')
    .post(passportJWT, RecordsController.addRecords);

router.route('/deleteRecords')
    .post(passportJWT, RecordsController.deleteRecords);

router.route('/editRecords')
    .post(passportJWT, RecordsController.editRecords);

module.exports = router;