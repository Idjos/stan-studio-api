const express = require('express');
const router = require('express-promise-router')();
const passport = require('passport');
const passportConf = require('../passport');

const UsersController = require('../controllers/users');
const passportSignin = passport.authenticate('local', {session:false});
const passportJWT = passport.authenticate('jwt', {session: false});

router.route('/signup')
    .post(UsersController.signUp);

router.route('/signIn')
    .post(passportSignin, UsersController.signIn);

module.exports = router;