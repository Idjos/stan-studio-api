const passport = require('passport');
const JwtStrategy = require('passport-jwt').Strategy;
const { ExtractJwt } = require('passport-jwt');
const LocalStrategy = require('passport-local').Strategy;
const {JWT_SECRET} = require('./configuration');
const config = require('./configuration/index');
const User = require('./models/users');

// json web tokens strategy
passport.use(new JwtStrategy({
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken('Authorization'),
    secretOrKey: JWT_SECRET
}, async (payload, done) =>{
    try {
        //find the users specified in token
        const user = await User.findById(payload.sub);

        //if user doesnt exists handle
        if (!user) {
            return done(null, false);
        }

        //return the user
        done(null, user);
    }
    catch (error) {
        done(error, false);
    }
}));

//local strategy
passport.use(new LocalStrategy({
    usernameField: 'email'
}, async (email, password, done) => {
    try {
        //find the user
        const user = await User.findOne({email});

    //if not
    if(!user) {
        return done(null, false);
    }

    //check password
    const isMatch = await user.isValidPassword(password);
    //if not, handle
    if (!isMatch) {
        return done(null, false);
    }

    //return the user
    done(null, user);
    }
    catch (error) {
        done(error, false);
    }
}));