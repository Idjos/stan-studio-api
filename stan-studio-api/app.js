const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');

const app = express();
app.use(cors());

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/stan-studio');

//Middlewares
app.use(morgan('dev'));

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({
    extended: true
}));

// parse application/json
app.use(bodyParser.json());

// Routes
app.use('/users', require('./routes/users'));
app.use('/records', require('./routes/records'));

//Start the server
const port = process.env.PORT || 3000;
app.listen(port);