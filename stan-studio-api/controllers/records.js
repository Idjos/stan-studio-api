const Record = require('../models/records');

module.exports = {

    getRecords: async (req, res, next) => {
        const foundRecords = await Record.find();
        res.status(200).json(foundRecords);
    },

    addRecords: async (req, res, next) => {

        const category = req.body.category;
        const sub_category = req.body.sub_category;
        const singer = req.body.singer;
        const song_name = req.body.song_name;
        const songs_list = req.body.songs_list;
        const year = req.body.year;
        const youtube = req.body.youtube;
        const mix_master = req.body.mix_master;
        const artist = req.body.artist;
        const instrumental = req.body.instrumental;

        console.log(category);

        //Create new record
        const newRecord = new Record({
            category: category,
            sub_category: sub_category,
            singer: singer,
            song_name: song_name,
            year: year,
            youtube: youtube,
            mix_master: mix_master,
            artist: artist,
            instrumental: instrumental,
            songs_list: songs_list
        });
        await newRecord.save();

        //Respond with token
        res.status(200).json({message: 'Uspešno sačuvano!'});
    },

    deleteRecords: async (req, res, next) => {
        console.log(req.body.id);
        const _id = req.body.id;
        // const foundRecord = await Record.findOneAndRemove({_id});
        Record.remove({_id}, function(err,removed) {

            res.status(200).json({message: 'Uspešno obrisano!'});
            // where removed is the count of removed documents
        });
    },

    editRecords: async (req, res, next) => {
        const sub_category = req.body.sub_category;
        const singer = req.body.singer;
        const song_name = req.body.song_name;
        const songs_list = req.body.songs_list;
        const year = req.body.year;
        const youtube = req.body.youtube;
        const mix_master = req.body.mix_master;
        const artist = req.body.artist;
        const instrumental = req.body.instrumental;


        //Create new record
        await Record.findOne({_id:req.body._id}, function(err,updated) {
            console.log('update', updated);

            updated.sub_category = 'Singl';
            updated.sub_category = sub_category;
            updated.singer= singer;
            updated.song_name= song_name;
            updated.year= year;
            updated.youtube= youtube;
            updated.mix_master= mix_master;
            updated.artist= artist;
            updated.instrumental= instrumental;
            updated.songs_list= songs_list;

            updated.save();
            res.status(200).json({message: 'Uspešno sačuvano!'});
        });
    }
};