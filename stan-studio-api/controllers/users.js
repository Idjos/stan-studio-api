const JWT = require('jsonwebtoken');
const User = require('../models/users');
const {JWT_SECRET} = require('../configuration');

signToken = user => {
    const token = JWT.sign({
        iss: 'CodeWorker',
        sub: user.id,
        iat: new Date().getTime(), //current time
        exp: new Date().setDate(new Date().getDate() + 1) //curent time + 1 day
    }, JWT_SECRET);
    console.log(user.id);
    return token;
}

module.exports = {
    signUp: async (req, res, next) => {
        //Email and password

        const email = req.body.email;
        const password = req.body.password;

        //check if there is user with the same email
        const foundUser = await User.findOne({email});
        if (foundUser) {
            return res.status(403).send('email is alredy in use')
        };

        //Create new user
        const newUser = new User({
            email: email,
            password: password
        });
        await newUser.save();

        //Generate token
        const token = signToken(newUser);

        //Respond with token
        res.status(200).json({token});
    },

    signIn: async (req, res, next) => {
        //need to generate token
        const token = signToken(req.user);
        res.status(200).json({token});
    }

};