const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const Schema = mongoose.Schema;

//Create a schema
const userSchema = new Schema({
    email: String,
    password: String,
});

userSchema.pre('save', async function (next) {
    try {
        // Generate a salt
        const salt = await bcrypt.genSalt(10);
        const password_hash = await bcrypt.hash(this.password, salt);
        this.password = password_hash;
        next();
    }
    catch (error) {
        next(error);
    }
});

userSchema.methods.isValidPassword = async function (newPassword) {
    try {
        return await bcrypt.compare(newPassword, this.password);
    }
    catch (error) {
        throw new Error(error);
    }
}

//Create a model
const User = mongoose.model('user', userSchema);

//export the model
module.exports = User;