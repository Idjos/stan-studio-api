const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create a schema
const recordSchema = new Schema({
    category: String,
    sub_category: String,
    singer: String,
    song_name: String,
    year: Number,
    youtube: String,
    mix_master: String,
    artist: String,
    instrumental: String,
    songs_list: String
});

//Create a model
const Record = mongoose.model('record', recordSchema);

//export the model
module.exports = Record;